---
layout: page
title: Shirts
permalink: /attend/shirts/
sponsors: true
---

We will be offering three cuts of shirt:

* The Straight Cut option is the Men's Staple Tee - [5001](https://www.ascolour.com.au/staple-tee-5001.html) and [5001B](https://www.ascolour.com.au/5001b-staple-tee-4xl-5xl.html)
* The Semi-Fitted option is the [Wo's Maple Tee - 4001](https://www.ascolour.com.au/4001-maple-tee.html)
* The Fitted option is the [Wo's Wafer Tee - 4002](https://www.ascolour.com.au/4002-wafer-tee.html)

## Sizing Information

* All measurements are in centimetres
* W = Width, L = Length

{% include shirt_sizes.html %}
