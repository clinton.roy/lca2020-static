---
layout: page
title: Miniconfs
permalink: /programme/miniconfs/
sponsors: true
---

## What are Miniconfs?

The first two days of linux.conf.au are made up of dedicated day-long streams focussing on single topics.
They are organised and run by volunteers and are a great way to kick the week off.

First introduced at linux.conf.au 2002, they are now a traditional element of the conference.
They were originally intended as an incubator -- both of future conferences and speakers.

Although delegates who present at miniconfs are not afforded speaker privileges at the main conference, speaking at a miniconf is a great way to gain experience, provide exposure for your project or topic, and raise your professional profile.

## Call for Sessions

{% include miniconf_cfs.md lead="Some miniconfs are still accepting proposals. Get in today so you don't miss out!" %}

To submit a proposal, create an account or login to view your [Dashboard](/dashboard/).
Following this, you will first need to create your Speaker Profile, then you can submit your proposal(s).
Each of the miniconfs will have a proposal type listed, and you are welcome to submit as many proposals as you like to as many miniconfs as you want.

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## Miniconfs at linux.conf.au 2020

{% include miniconf_grid.html %}
