---
layout: page
title: Games and FOSS Miniconf
permalink: /programme/miniconfs/games/
sponsors: true
---

<p class="lead">
Organised by Tim Nugent and Paris Buttfield-Addison
</p>

## When

Monday 13 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="games-miniconf" day="monday" %}

## About

Start your game neurons firing, we're back!
We took a bit of break to get our thoughts back in line, but now we're ready once more: it's the linux.conf.au Games Miniconf!

FOSS and games have been getting closer and closer for a while now, but there is still more to do, and more knowledge to be shared.
This miniconf will be a single day exploring the interaction of games, free and open source software, and their communities and developers.

If you have something to sat about the role of open source in gaming, or you've got a passion for game development, FOSS, community, or playing games, then this miniconf will be of interest to you.

We are seeking talks on _anything_ that looks into the interaction between games, game development, and free and open source software.

Potential talk topics include (but are not limited to):

- Fully Open Source or Free games
- Free and Open Source game engines and tools
- Pros and cons of using the above
- Legal or business implications of game development using FOSS
- The history of Open Source in games and game development
- Open sourcing your game
- Creating open hooks into proprietary games
- Diversity in open source and game development, what we can learn from each other
- What the FOSS and LCA community can learn from the game developers
- Using proprietary engines while maintain a FOSS spirit
- What game developers can learn from the LCA and FOSS community
- Open game hardware and peripherals
- Playing games on open hardware and platforms
- Community management and interaction difference between the FOSS and games community
- Open source art and music for games
- FOSS and board games
- Anything FOSS and Game related you can think of!

### Format

The linux.conf.au Games Miniconf is open for submissions for the following talk types:

- 15 minute presentations
- 5 minute demos and micro-talks

Due to the single day, single track, nature of the miniconf we are expecting the majority of acceptances to be 15 minute presentations. We can possibly accept a limited number of slightly longer presentations (30 minutes) on a case-by-case basis.

We are also seeking people willing to set up their game to let people play with it during the lunch break. Built a game that has some FOSS technologies or art? Come and show us!

If you have a compelling idea and do need more time, or have an idea beyond a presentation or a demonstration let us know and we will see what we can do.

The goal of this miniconf is to build on the success of the previous years Games Miniconfs to continue promoting the common interests of both FOSS and game development, to more deeply investigate the challenges FOSS faces as it pursues greater representation in games and game development, and simply to talk about games. Help us make these goals happen!

### Talk submission

To submit your talk please use the LCA submission system.

If you are submitting a demo of a game, tool, or project make sure that your submission contains a link where we can preview it (a video is fine), otherwise we won't be able to approve it.

We **strongly** encourage first-time speakers, as well as seasoned speakers from all walks of life: all ages, genders, nationalities, ethnicities, backgrounds, religions, and abilities.

Like the main LCA conference, we respect and encourage diversity at our miniconf.

If you would like any assistance with creating a proposal, don't hesitate to ask one of the organisers: they can be reached at lcagames@secretlab.topicbox.com

Please note, to present or attend the miniconf you will need to be a registered attendee of LCA. There are miniconf only tickets available, but we recommend you attend the entire conference as there are amazing people in attendance, it would be a shame to miss them.

Unfortunately we do not have sponsorship to fund attendance, but if you cannot attend and wish to please get in contact and we will do our very best to make this a reality.

### Contact

If you have any questions or need to get in contact with us, please don't hestitate to send us an email, lcagames@secretlab.topicbox.com
