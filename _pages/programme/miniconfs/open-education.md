---
layout: page
title: Open Education Miniconf
permalink: /programme/miniconfs/open-education/
sponsors: true
---

<p class="lead">
Organised by Arjen Lentz and Roland Gesthuizen
</p>

## When

Monday 13 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="open-education-miniconf" day="monday" %}

## About

A day on using open tools, open source and creative commons thinking in schools and other education environments.
How open source hardware and software, open data and standards, can be a force for good change and really enable students' engagement with learning - reducing or removing gender, financial and other barriers.
