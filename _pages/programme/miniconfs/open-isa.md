---
layout: page
title: Open ISA (RISC-V, OpenPOWER, etc) Miniconf
permalink: /programme/miniconfs/open-isa/
sponsors: true
---

<p class="lead">
Organised by Alistair Francis & Hugh Blemings
</p>

## When

Monday 13 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="open-isa-miniconf" day="monday" %}

## About

The recent phenomenal growth of RISC-V proves that Open CPU architectures are no longer only an academic project but are becoming a serious contender among commercial processor architectures.

Thanks to the active contributions from both industry pioneers and academic researchers, we are entering into an exciting era of open source hardware designs ranging from micro-controllers to server class enterprise systems backed by a growing open source ecosystem, constantly evolving and improving.

A growing variety of hardware based on Open ISAs like RISC-V, OpenPOWER and others are becoming available, allowing a larger choice of end applications beyond embedded micro-controllers.  At the chip level Open ISAs allow new extensions and specialised compute functions to be added - something we are now seeing in production systems.

Accordingly, we’re pleased to announce the first Open-ISA miniconf at linux.conf.au

The Open-ISA track will discuss the state of the art, include key areas/topics such as secure boot, the RISC-V 32-bit glibc port, RISC-V Hypervisor extensions, the "Microwatt" OpenPOWER implementation and more.

It will also provide an opportunity to increase developer participation in code review/patch submissions for all open source projects as we discuss support for Open ISAs in the Linux kernel and other open source projects.
