---
layout: page
title: System Administration Miniconf
permalink: /programme/miniconfs/system-administration/
sponsors: true
---

<p class="lead">
Organised by Simon Lyall and Ewen McNeill
</p>

## When

Monday 13 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="sysadmin-miniconf" day="monday" closed="True" %}

## About

The [Systems Administration Miniconf](https://sysadmin.miniconf.org/) focuses on professional management of real-world Linux and open source environments, both large and small.
The Miniconf aims to include a diverse range of tools and techniques that will help keep your entire environment functioning smoothly, and accomplish more with less effort.
An important goal will be to provide talks directly useful to professional Linux administrators.

The [Systems Administration Miniconf](https://sysadmin.miniconf.org/)
has run at 13 previous Linux.Conf.Au conferences.  More information,
and slides/recordings of previous presentations, can be found on the [Systems
Administration Miniconf website](https://sysadmin.miniconf.org/) at:

https://sysadmin.miniconf.org/

A detailed [Call for Proposals](https://sysadmin.miniconf.org/cfp20.html)
can be found at:

https://sysadmin.miniconf.org/cfp20.html
