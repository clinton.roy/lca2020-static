---
layout: post
title: Conference schedule now available
---

<p class="lead">We are proud to announce the conference schedule for linux.conf.au 2020.</p>

The Session Selection Committee and Organising Team have pulled together an exciting schedule for linux.conf.au 2020.
The first two days include 12 miniconfs covering an array of interesting subject areas.
Following this, we will be running 80 talks plus eight tutorials across Wednesday, Thursday and Friday of the main conference.
The sessions cover a wide variety of topics, including open source software, open hardware, Linux kernel, documentation and community.
This is overlayed with our theme for linux.conf.au 2020 of "Who's Watching", focusing on security, privacy and ethics.

You can now read the [full schedule](/schedule/).

## Miniconfs

As you know, we will be running 12 [miniconfs](/programme/miniconfs/) across the Monday and Tuesday of linux.conf.au 2020.
All of the miniconfs are currently accepting proposals for sessions, so we encourage you to submit some talks today.
You have until Sunday 8 December [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth) to submit your proposal.
If you require additional time to make arrangements to attend the conference, please submit your proposal by Sunday 17 November to be considered for early acceptance.

## Tickets

Now is a great time to purchase your [tickets](/attend/tickets/) to linux.conf.au 2020!
Tickets are on sale now, with early bird prices still available until the end of the month.
Get in quick to make sure you do not miss out.

If you have any questions about the schedule or anything to do with linux.conf.au 2020, you can contact us via [email](mailto:contact@lca2020.linux.org.au).
