---
layout: post
title: "Keynote announcement: Sean Brady"
card: keynote_sean.7544db1c.png
---

<p class="lead">Our third keynote speaker for LCA2020 is Dr Sean Brady.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/sean_brady.jpg" width="250">

Dr Sean Brady is a forensic engineer, specialising in investigating the causes of engineering collapses and failures.
He believes Open Source is important because it allows the best tools to get into the best hands to use them.

Sean's keynote will look at how possessing expertise always seems like a good thing - something that makes us less prone to failure.
In this presentation Sean will challenge delegates to dismantle how they use their expertise (regardless of profession), and show them that, as well as making us more specialised, it also dangerously narrows how we see the world.
For delegates who believe that their technical expertise is their most important asset, he is going to make delegates have a deep rethink...

## A bit about Sean

Dr Sean Brady is a forensic engineer who specialises in investigating the causes of engineering collapses and failures.
Sean is interested in role played by human and system factors in failure, and he has recently completed a review for the Queensland Government into the causes of fatalities in the Queensland mining industry since 2000.
He is a director of the Society of Construction Law Australia, a member of the Singapore International Mediation Centre's Panel of Experts, and a visiting lecturer at a number of universities.

## Interested?

If you’re interested to hear Sean speak make sure you register for linux.conf.au 2020.
You can purchase your [tickets](/attend/tickets/) today.
